// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { getters } from 'apps/address-book/src/store/getters';
import { defaultAddressList } from './mockData';

describe('Address addresses Getters', () => {
  it('returns poodles', () => {
    const actual = getters.addresses({ addresses: defaultAddressList });

    expect(actual).toEqual(defaultAddressList);
  });
});

describe('Address get by id Getters', () => {
  it('returns poodles', () => {
    const actual = getters.getAddress({ addresses: defaultAddressList })(
      defaultAddressList[0].id
    );

    expect(actual).toEqual(defaultAddressList[0]);
  });
});
