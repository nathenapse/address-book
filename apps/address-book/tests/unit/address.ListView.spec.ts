import { createLocalVue, RouterLinkStub, shallowMount } from '@vue/test-utils';
import ListView from '../../src/features/address/views/ListView.vue';
import Vuex from 'vuex';
import { defaultAddressList } from './mockData';
import AddressItem from '../../src/features/address/components/AddressItem.vue';

const localVue = createLocalVue();

localVue.use(Vuex);

describe('Address/views/ListView.vue', () => {
  let getters;
  let store;

  beforeEach(() => {
    getters = {
      addresses: () => [...defaultAddressList],
    };
    store = new Vuex.Store({
      getters,
    });
  });
  it('Render Address ListVue', () => {
    const wrapper = shallowMount(ListView, {
      store,
      localVue,
      stubs: {
        RouterLink: RouterLinkStub,
      },
    });
    expect(wrapper.findComponent(RouterLinkStub).props().to['name']).toBe(
      'AddressCreateView'
    );
    expect(wrapper.text()).toMatch('New');
  });
  it('Lists all Items', () => {
    const wrapper = shallowMount(ListView, {
      store,
      localVue,
      stubs: {
        RouterLink: RouterLinkStub,
      },
    });
    expect(wrapper.findAllComponents(AddressItem).length).toBe(
      defaultAddressList.length
    );
  });
});
