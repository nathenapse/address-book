import {
  createLocalVue,
  mount,
  RouterLinkStub,
  shallowMount,
} from '@vue/test-utils';
import UpdateView from '../../src/features/address/views/UpdateView.vue';
import Vuex from 'vuex';
import { defaultAddressList } from './mockData';
import AddressForm from '../../src/features/address/components/AddressForm.vue';
import Vue from 'vue';

const localVue = createLocalVue();

localVue.use(Vuex);

describe('Address/views/UpdateView.vue', () => {
  let actions;
  let getters;
  let store;
  const $route = { params: { id: defaultAddressList[0].id } };
  const $router = [];

  beforeEach(() => {
    actions = {
      UPDATE_ADDRESS: jest.fn(),
    };
    getters = {
      addresses: () => [...defaultAddressList],
      getAddress: () => () => defaultAddressList[0],
    };
    store = new Vuex.Store({
      actions,
      getters,
    });
  });
  it('Renders', () => {
    const wrapper = mount(UpdateView, {
      store,
      localVue,
      mocks: {
        $route,
        $router,
      },
    });
    expect(wrapper.text()).toContain('Update Address');
  });
  it('Shows Item not found if param is not passed', async () => {
    const $routeWithoutParam = { params: {} };
    const wrapper = shallowMount(UpdateView, {
      store,
      localVue,
      stubs: {
        RouterLink: RouterLinkStub,
      },
      mocks: {
        $route: $routeWithoutParam,
        $router,
      },
    });
    expect(wrapper.text()).toContain('Item Not Found');
    expect(wrapper.findComponent(RouterLinkStub).props().to['name']).toBe(
      'AddressListView'
    );
  });
  it('Calls appropriate method for events', async () => {
    const wrapper = shallowMount(UpdateView, {
      store,
      localVue,
      mocks: {
        $route,
        $router,
      },
    });
    wrapper.findComponent(AddressForm).vm.$emit('on-submit', {
      ...defaultAddressList[0],
      email: 'nathanmuler@tesm.com',
    });
    expect(actions.UPDATE_ADDRESS).toBeCalled();
    await Vue.nextTick();
    expect($router.length).toBe(1);
    expect($router[0]['name']).toBe('AddressListView');
  });
});
