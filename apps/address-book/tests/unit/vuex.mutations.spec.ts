// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { AddressModel } from 'apps/address-book/src/models/Address';
import { mutations } from '../../src/store/mutations';
import { defaultAddressList, newAddress } from './mockData';

describe('mutations', () => {
  test('Add address to List', () => {
    const defaultAddresses: AddressModel[] = [...defaultAddressList];
    const itemToAdd: AddressModel = {
      ...newAddress,
      id: Date.now(),
      updated_at: Date.now(),
    };
    const state = {
      addresses: [...defaultAddressList],
    };
    mutations.ADD_ADDRESS(state, itemToAdd);
    defaultAddresses.unshift(itemToAdd);
    expect(state.addresses.length).toBe(defaultAddresses.length);
    expect(JSON.stringify(state.addresses)).toBe(
      JSON.stringify(defaultAddresses)
    );
  });
  test('Delete address with id', () => {
    const defaultAddresses = [...defaultAddressList];
    const state = {
      addresses: [...defaultAddressList],
    };
    mutations.DELETE_ADDRESS(state, defaultAddressList[0].id);
    defaultAddresses.shift();
    expect(state.addresses.length).toBe(defaultAddresses.length);
    expect(JSON.stringify(state.addresses)).toBe(
      JSON.stringify(defaultAddresses)
    );
  });

  test('Update address', () => {
    const defaultAddresses = [...defaultAddressList];
    const state = {
      addresses: [...defaultAddressList],
    };
    const emailToUpdate = 'test@email.test';
    const itemToUpdate = { ...defaultAddresses[0], email: emailToUpdate };
    mutations.UPDATE_ADDRESS(state, itemToUpdate);
    expect(state.addresses.length).toBe(defaultAddresses.length);
    expect(state.addresses[0].email).toBe(emailToUpdate);
  });
});
