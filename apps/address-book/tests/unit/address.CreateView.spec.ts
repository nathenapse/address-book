import {
  createLocalVue,
  mount,
  RouterLinkStub,
  shallowMount,
} from '@vue/test-utils';
import CreateView from '../../src/features/address/views/CreateView.vue';
import Vuex from 'vuex';
import { newAddress } from './mockData';
import AddressForm from '../../src/features/address/components/AddressForm.vue';
import Vue from 'vue';

const localVue = createLocalVue();

localVue.use(Vuex);

describe('Address/views/CreateView.vue', () => {
  let actions;
  let store;
  const $route = {};
  const $router = [];

  beforeEach(() => {
    actions = {
      ADD_ADDRESS: jest.fn(),
    };
    store = new Vuex.Store({
      actions,
    });
  });
  it('Renders', () => {
    const wrapper = mount(CreateView, {
      store,
      localVue,
      stubs: {
        RouterLink: RouterLinkStub,
      },
    });
    expect(wrapper.text()).toContain('Add Address');
  });
  it('Calls appropriate method for events', async () => {
    const wrapper = shallowMount(CreateView, {
      store,
      localVue,
      stubs: {
        RouterLink: RouterLinkStub,
      },
      mocks: {
        $route,
        $router,
      },
    });
    wrapper.findComponent(AddressForm).vm.$emit('on-submit', newAddress);
    expect(actions.ADD_ADDRESS).toBeCalled();
    await Vue.nextTick();
    expect($router.length).toBe(1);
    expect($router[0]['name']).toBe('AddressListView');
  });
});
