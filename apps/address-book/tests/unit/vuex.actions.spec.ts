/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { AddressModel } from 'apps/address-book/src/models/Address';
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { MutationTypes } from 'apps/address-book/src/store/mutations';
import { actions } from '../../src/store/actions';
import { defaultAddressList, newAddress } from './mockData';

describe('actions', () => {
  test('Add address action', () => {
    const commit: any = jest.fn() as any;
    jest
      .spyOn(global.Date, 'now')
      .mockImplementationOnce(() => Date.parse('2020-02-14'));
    const itemToAdd: AddressModel = { ...newAddress };
    actions.ADD_ADDRESS({ commit } as any, itemToAdd);

    expect(commit).toHaveBeenCalledWith(MutationTypes.ADD_ADDRESS, {
      ...itemToAdd,
      id: Date.parse('2020-02-14'),
      updated_at: Date.parse('2020-02-14'),
    });
  });
  test('Update address action', () => {
    const commit: any = jest.fn() as any;
    jest
      .spyOn(global.Date, 'now')
      .mockImplementationOnce(() => Date.parse('2021-02-14'));
    const itemToUpdate: AddressModel = { ...defaultAddressList[0] };
    actions.UPDATE_ADDRESS({ commit } as any, itemToUpdate);

    expect(commit).toHaveBeenCalledWith(MutationTypes.UPDATE_ADDRESS, {
      ...itemToUpdate,
      updated_at: Date.parse('2021-02-14'),
    });
  });
  test('Delete address action', () => {
    const commit: any = jest.fn() as any;
    const itemToUpdate: AddressModel = { ...defaultAddressList[0] };
    actions.DELETE_ADDRESS({ commit } as any, itemToUpdate);

    expect(commit).toHaveBeenCalledWith(
      MutationTypes.DELETE_ADDRESS,
      itemToUpdate.id
    );
  });
});
