export const defaultAddressList = [
  {
    firstName: 'Nathan',
    lastName: 'Mulugeta',
    email: 'nahomuller@gmail.com',
    country: 'AE',
    id: 1624262344971,
    updated_at: 1624262992676,
  },
  {
    firstName: 'Nardos',
    lastName: 'Mulugeta',
    email: 'nardi@gmail.com',
    country: 'BF',
    id: 1624262321154,
    updated_at: 1624262718383,
  },
];

export const newAddress = {
  firstName: 'Nahom',
  lastName: 'Mulugeta',
  email: 'nahomuller@gmail.com',
  country: 'BF',
};
