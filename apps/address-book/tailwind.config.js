module.exports = {
  purge: { content: ['apps/address-book/src/**/*.{vue,js,ts,jsx,tsx}'] },
  darkMode: false,
  theme: {
    extend: {},
  },
  variants: {
    extend: {
      opacity: ['disabled'],
      backgroundColor: ['disabled'],
      cursor: ['disabled'],
    },
  },
  plugins: [],
};
