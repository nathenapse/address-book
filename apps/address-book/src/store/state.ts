import { AddressModel } from '../models/Address';

export const state: { addresses: AddressModel[] } = {
  addresses: [],
};

export type State = typeof state;
