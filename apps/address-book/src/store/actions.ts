import { ActionContext, ActionTree } from 'vuex';
import { AddressModel } from '../models/Address';
import { Mutations, MutationTypes } from './mutations';
import { State } from './state';

export enum ActionTypes {
  ADD_ADDRESS = 'ADD_ADDRESS',
  UPDATE_ADDRESS = 'UPDATE_ADDRESS',
  DELETE_ADDRESS = 'DELETE_ADDRESS',
}

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<State, State>, 'commit'>;

export interface Actions {
  [ActionTypes.ADD_ADDRESS](
    { commit }: AugmentedActionContext,
    payload: AddressModel
  ): void;
  [ActionTypes.UPDATE_ADDRESS](
    { commit }: AugmentedActionContext,
    payload: AddressModel
  ): void;
  [ActionTypes.DELETE_ADDRESS](
    { commit }: AugmentedActionContext,
    payload: AddressModel
  ): void;
}

export const actions: ActionTree<State, State> & Actions = {
  [ActionTypes.ADD_ADDRESS]({ commit }, payload: AddressModel) {
    const date = Date.now();
    const data: AddressModel = {
      ...payload,
      id: date,
      updated_at: date,
    };
    commit(MutationTypes.ADD_ADDRESS, data);
  },
  [ActionTypes.UPDATE_ADDRESS]({ commit }, payload: AddressModel) {
    const data: AddressModel = { ...payload, updated_at: Date.now() };
    commit(MutationTypes.UPDATE_ADDRESS, data);
  },
  [ActionTypes.DELETE_ADDRESS]({ commit }, payload: AddressModel) {
    commit(MutationTypes.DELETE_ADDRESS, payload.id);
  },
};
