import { MutationTree } from 'vuex';
import { AddressModel } from '../models/Address';
import { State } from './state';

export enum MutationTypes {
  ADD_ADDRESS = 'ADD_ADDRESS',
  UPDATE_ADDRESS = 'UPDATE_ADDRESS',
  DELETE_ADDRESS = 'DELETE_ADDRESS',
}

export type Mutations<S = State> = {
  [MutationTypes.ADD_ADDRESS](state: S, data: AddressModel): void;
  [MutationTypes.UPDATE_ADDRESS](state: S, data: AddressModel): void;
  [MutationTypes.DELETE_ADDRESS](state: S, data: number): void;
};

export const mutations: MutationTree<State> & Mutations = {
  [MutationTypes.ADD_ADDRESS](state, data: AddressModel): void {
    state.addresses.unshift(data);
  },
  [MutationTypes.UPDATE_ADDRESS](state, data: AddressModel): void {
    const existsAtIndex = state.addresses.findIndex(
      (address) => address.id === data.id
    );
    if (existsAtIndex !== -1) {
      state.addresses[existsAtIndex] = data;
    }
  },
  [MutationTypes.DELETE_ADDRESS](state, id: number): void {
    state.addresses = state.addresses.filter((address) => address.id !== id);
  },
};
