import { GetterTree } from 'vuex';
import { AddressModel } from '../models/Address';
import { State } from './state';

export type Getters = {
  addresses(state: State): AddressModel[];
  getAddress(state: State): (id: number) => AddressModel;
};

export const getters: GetterTree<State, State> & Getters = {
  addresses: (state) => {
    return state.addresses;
  },
  getAddress: (state) => (id: number) => {
    return state.addresses.find((address) => address.id === id);
  },
};
