export interface AddressModel {
  id?: number;
  firstName: string;
  lastName: string;
  email: string;
  country: string;
  updated_at?: number;
}
