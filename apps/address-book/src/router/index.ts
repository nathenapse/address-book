import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import ListView from '../features/address/views/ListView.vue';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  { path: '/', redirect: '/address' },
  {
    path: '/address',
    component: Home,
    children: [
      {
        path: '',
        name: 'AddressListView',
        component: ListView,
      },
      {
        path: 'create',
        name: 'AddressCreateView',
        component: () =>
          import(
            /* webpackChunkName: 'AddressCreateView' */ '../features/address/views/CreateView.vue'
          ),
      },
      {
        path: 'update/:id',
        name: 'AddressUpdateView',
        component: () =>
          import(
            /* webpackChunkName: 'AddressUpdateView' */ '../features/address/views/UpdateView.vue'
          ),
      },
    ],
  },
  {
    path: '*',
    component: () =>
      import(/* webpackChunkName: 'NotFoundView' */ '../views/404.vue'),
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ '../views/About.vue'),
  // },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
