import { VueConstructor } from 'vue/types/umd';
import * as countryList from 'country-list';

export const CountryName = {
  install(Vue: VueConstructor) {
    Vue.filter('countryName', CountryNameFilter);
  },
};

export const CountryNameFilter = (value: string): string => {
  return countryList.getName(value);
};
